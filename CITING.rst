When using this software, please cite:

E. Van Veen, G. Slotman, S. Yuan, 
"Tipsi: a tight-binding propagation simulator" [unpublished]
